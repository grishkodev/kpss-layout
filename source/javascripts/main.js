import Swiper from 'swiper';
import svg4everybody from 'svg4everybody';

let $window = $(window);
let $document = $(document);
let $html = $(document.documentElement);
let $body = $(document.body);

let colorRegion = '#EC3839';
let focusRegion = '#EC3839';

function showMap(data) {
  $('#vmap').vectorMap({
    map: 'russia_ru',
    backgroundColor: '#F4EBE4',
    borderColor: '#ffffff',
    borderWidth: 1,
    color: colorRegion,
    selectedColor: focusRegion,
    colors: data.inactive,
    hoverOpacity: 0.8,
    enableZoom: true,
    showTooltip: true,

    onLabelShow: function(event, label, code) {
      let name = '<div class="tooltip-name">' + label.text() + '</div>';
      let text = '<div class="tooltip-text">' + data.labeltext + '</div>';
      if (!data.inactive[code]) {
        status = '<div class="tooltip-status">Свободна</div>';
      } else {
        status = '<div class="tooltip-status tooltip-status--false">Заната</div>';
      }
      label.html(name + status + text);
    },
    onRegionClick: function(event, code, region) {
      event.preventDefault();
    }
  });
};

$document.ready(() => {

  svg4everybody();

  let mySwiper = new Swiper ('.swiper-container', {
    loop: true,
    pagination: {
      el: '.swiper-pagination',
      clickable: true
    }
  });

  if ($('#vmap').length) {
    $.getJSON( "assets/js/data.json", function(json) {
      let mapData = json;
      $.fn.vectorMap('addMap', 'russia_ru', mapData);
      showMap(mapData);
    });
  }

  $('.hamburger').on('click', function(event) {
    $(this).toggleClass('is-active');
    $('.mobile-menu').toggleClass('is-open');
  });

  $('.accordion__item-title').on('click', function(event) {
    $(this).parent('.accordion__item').toggleClass('is-open');
  });

});
