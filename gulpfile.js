var	gulp = require('gulp'),
	pug = require('gulp-pug'),
	notify = require('gulp-notify'),
	cleancss = require('gulp-clean-css'),
	autoprefixer = require('gulp-autoprefixer'),
	rename = require('gulp-rename'),
	sass = require('gulp-sass'),
	connect = require('gulp-connect'),
	livereload = require('gulp-livereload'),
	uglify = require('gulp-uglify'),
	browserify = require('browserify'),
	babelify = require('babelify'),
	source = require('vinyl-source-stream'),
	buff = require('vinyl-buffer'),
	svgSprite = require('gulp-svg-sprite'),
	svgmin = require('gulp-svgmin'),
	cheerio = require('gulp-cheerio'),
	replace = require('gulp-replace');

var site = 'public',
	inputCss = 'source/stylesheets/sass/*.{sass,scss}',
	outputCss = 'source/stylesheets/css',
	outputMinCss = 'public/assets/css',
	inputJs = 'source/javascripts/main.js',
	outputMinJs = 'public/assets/js',
	inputSass = 'source/stylesheets/sass/**/*.{sass,scss}',
	inputPug = 'source/views/pages/*.pug',
	inputSvgInline = 'source/images/svg-inline/*.svg',
	inputSvgBg = 'source/images/svg-bg/*.svg',
	outputSvg = 'public/assets/images/icons';

gulp.task('connect', function() {
	connect.server({
		root: site,
		livereload: true,
		port: 8081
	});
});

gulp.task('views', function buildHTML() {
	return gulp.src(inputPug)
		.pipe(pug({
			pretty: true
		}))
		.on('error', notify.onError(function (error) {
			return 'An error occurred while compiling Pug.\nLook in the console for details.\n' + error;
		}))
		.pipe(gulp.dest(site));
});

gulp.task('html', function(){
	gulp.src(site+'/*.html')
		.pipe(connect.reload());
});

gulp.task('styles', function() {
	gulp.src(inputCss)
		.pipe(sass({
			outputStyle: 'expanded'
		}))
		.on('error', function(err) {
	        notify().write(err);
	        this.emit('end');
	    })
		.pipe(autoprefixer('last 99 version'))
		.pipe(gulp.dest(outputCss));
});

gulp.task('cssmin', function() {
	return gulp.src(outputCss+'/*.css')
		.pipe(rename({suffix: '.min'}))
		.pipe(cleancss())
		.pipe(gulp.dest(outputMinCss))
		.pipe(connect.reload());
});

gulp.task('js', function() {
	return browserify({entries: inputJs, debug: true})
		.transform("babelify", { presets: ["env"] })
		.bundle()
		.pipe(source('main.js'))
		.pipe(buff())
	    .pipe(uglify())
		.on('error', notify.onError(function (error) {
			return 'Javascript minification error.\nLook in the console for details.\n' + error;
		}))
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest(outputMinJs))
		.pipe(connect.reload());
});

gulp.task('svg-inline', function () {
	return gulp.src(inputSvgInline)
		.pipe(svgmin({
			js2svg: {
				pretty: true
			}
		}))
		.pipe(replace('&gt;', '>'))
		.pipe(svgSprite({
			mode: {
				symbol: {
					sprite: "../sprite-inline.svg",
					render: {
						scss: {
							dest:'../../../../../source/stylesheets/sass/base/sprites/_svg-inline.scss',
							template: 'source/stylesheets/sass/base/sprites/_template-inline.scss'
						}
					}
				}
			}
		}))
		.pipe(gulp.dest(outputSvg));
});

gulp.task('svg-bg', function () {
	return gulp.src(inputSvgBg)
		.pipe(svgmin({
			js2svg: {
				pretty: true
			}
		}))
		.pipe(cheerio({
            run: function ($) {
				$('[style]').removeAttr('style');
            },
            parserOptions: { xmlMode: true }
        }))
		.pipe(replace('&gt;', '>'))
		.pipe(svgSprite({
			shape: {
				spacing: {
					padding: 5
				}
			},
			mode: {
				css: {
					sprite: "../sprite-bg.svg",
					bust: false,
					render: {
						scss: {
							dest:'../../../../../source/stylesheets/sass/base/sprites/_svg-bg.scss',
							template: 'source/stylesheets/sass/base/sprites/_template-bg.scss'
						}
					}
				}
			},
			variables: {
				mapname: "icons"
			}
		}))
		.pipe(gulp.dest(outputSvg));
});

gulp.task('watch', function() {
	gulp.watch(inputJs, ['js']);
	gulp.watch(outputCss+'/*.css', ['cssmin']);
	gulp.watch(inputCss, ['styles']);
	gulp.watch(inputSass, ['styles']);
	gulp.watch(inputSvgInline, ['svg-inline']);
	gulp.watch(inputSvgBg, ['svg-bg']);
	gulp.watch('source/views/**/*.pug', ['views']);
	gulp.watch(site+'/*.html', ['html']);
});

gulp.task('dev', ['connect', 'views', 'html', 'styles', 'cssmin', 'js', 'svg-inline', 'svg-bg', 'watch'], function() {

});
